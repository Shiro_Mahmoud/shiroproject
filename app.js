var createError = require('http-errors');
var express = require('express');
var session = require('express-session')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var config = require('./config');

var mongo = require('mongodb');
var monk = require('monk');
var db = monk(config.mongodbConf.hostDB);

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authsRouter = require('./routes/auths');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.use('/users', express.static(path.join(__dirname, 'views')));
app.use('/auths', express.static(path.join(__dirname, 'views')));

app.set('view engine', 'html');
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/users',express.static(path.join(__dirname, 'public')));
app.use('/auths',express.static(path.join(__dirname, 'public')));

// Session to save data
app.use(session({
  secret: 'ShiroOokami',
  resave: true,
  saveUninitialized: true
}));

// Make our db accessible to our router
app.use(function(req,res,next){
  req.db = db;
  next();
});

// redirect
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auths', authsRouter);

// error handler
app.use(function( req, res, next) {

  // render the error page
  if(res.status = 404)
    return res.sendFile(path.join(__dirname + '/views/404.html'));
  else if(err.status = 500)
    return res.sendFile(path.join(__dirname + '/views/500.html'));
    
});

module.exports = app;
