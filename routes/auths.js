var express = require('express');
var router  = express.Router();
var path    = require('path');
var uniqid  = require('uniqid');
var mongo   = require('mongodb');
var md5 = require('md5');

var Cryptr  = require('cryptr');
const cryptr = new Cryptr('keepItSecret');
//const decrypted_pass = cryptr.decrypt(log_pass);

/* POST signup function */
router.post('/signup', function(req, res, next) {
  var u_name  = req.body.name;
  var u_pass  = req.body.pass;
  var u_email = req.body.email;
  var u_avatar= req.body.avatar;
  var u_phone = req.body.phone;
  var u_gender= req.body.gender;
  //const encrypted_pass = cryptr.encrypt(u_pass);
  if(u_name && u_pass && u_email && u_avatar && u_phone && u_gender){
    var user_data = {
      "u_name"  : u_name,
      "u_email" : u_email,
      "u_avatar": u_avatar,
      "u_phone" : u_phone,
      "u_gender": u_gender,
      "u_encrypted_pass" : md5(u_pass) //encrypted_pass
    }
    saveLogInData(user_data, req, res);
  }else
    return res.send({res: "missing_data"});
});

/* POST login function */
router.post('/login', function(req, res, next) {
  var log_email= req.body.email;
  var log_pass = req.body.pass;
  if(log_email && log_pass){
    var log_data = {
      "email": log_email,
      "pass" : md5(log_pass)
    };
    userAuth(log_data, req, res);
  }else
    return res.send({res: "missing_data"});
});

/* POST forget with just a token function */
router.post('/forget-token', function(req, res, next) {
  return res.send("hi");
});

/* POST forget with token link function */
router.post('/forget-link', function(req, res, next) {
  return res.send("hi");
});

/* POST reset password using token function */
router.post('/reset-token', function(req, res, next) {
  return res.send("hi");
});

/* POST reset password using link function */
router.get('/reset-link/:token', function(req, res, next) {
  var project_id = req.params.token;
  return res.send(project_id);
});



var email = require("./mailfunction");
let opjEmail = new email();

/* test get */
router.post('/test', async function(req, res, next) {
  console.log("x")
   var x = await opjEmail.verifyCheck('ma7moudeyada77@gmail.com');
  return res.send(x);
});




/* find promise */
function getFindResult(query, collection) {
  return new Promise(function(resolve, reject) {
    collection.find(query,function(err, result) {
      if (err) reject(err);
      else{
        resolve(result);  
      }
    });
  }); 
}

/* insert promise */
function getInsertResult(data, collection) {
  return new Promise(function(resolve, reject) {
    /*Submit user data to the DB*/
    collection.insert(data, function (err, doc) {
      if (err) reject (err);
      else{
          resolve(doc);
      }
    });
  }); 
}

async function saveLogInData(u_data, req, res){
  /*Set our collection*/
  var users_coll = req.db.get('users');
  var query = {"u_email" : u_data.u_email };
  var exist_ex = [];
  var reg_ex;
  try {
    exist_ex = await getFindResult(query, users_coll);
    if(exist_ex[0]){
      console.log({db_res: exist_ex});
      return res.send({res: "user_exist"});
    }
    else{
      reg_ex = await getInsertResult(u_data, users_coll);
      if(reg_ex){
        console.log({db_res: reg_ex});
        return res.send({res: "user_added"});
      }
    }
  }
  catch (err) {
    console.log(err);
  }
}

async function userAuth(log_data, req, res){
  var query = {"u_email" : log_data.email };
  var log_coll = req.db.get('users');  
  var chek_ex = [];
  try {
    chek_ex = await getFindResult(query, log_coll);
    if(chek_ex[0]){
      console.log(chek_ex);
      if(chek_ex[0].u_email == log_data.email){
        var encrypted_pass = chek_ex[0].u_encrypted_pass;
        if(encrypted_pass == log_data.pass){
          req.session.login_auth = "logged";
          return res.send({res: "user_logged"});
        }else
          return res.send({res: "false_user_pass"});        
      }else
        return res.send({res: "false_user_pass"});
    }else
      return res.send({res: "false_user_pass"});
  }
  catch (err) {
    console.log(err);
  }
}

module.exports = router;