var express = require('express');
var router  = express.Router();
var path    = require('path');
var mongo   = require('mongodb');
var monk    = require('monk');
var config  = require('./../config');
var db      = monk(config.mongodbConf.hostDB);
ObjectID    = mongo.ObjectID;

var AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: config.AWSconf.accessKeyId,
    secretAccessKey: config.AWSconf.secretAccessKey,
    region: config.AWSconf.region
});


var _Ccemail          = 'ma7moudeyada77@gmail.com';
var _replyToAddresses = 'ma7moudeyada77@gmail.com';
var _toAddresses;
var _sourceAddress    = 'ma7moudeyada77@gmail.com';
var _textFormatBody   = 'we received your data is under revision';
var _emailSubject     = 'Your Project Verify';

var eMail = class eMail{

  sendMail(Ccemail, replyToAddresses, toAddresses, sourceAddress, textFormatBody, emailSubject, reset_code){
    console.log(Ccemail, replyToAddresses, toAddresses, sourceAddress, textFormatBody, emailSubject, reset_code);
    var emailData = {
        Destination: { 
            //CcAddresses: [Ccemail],
            ToAddresses: [toAddresses]
        },
        Message: {
            Body: {
                /*
                Html: {
                Charset: "UTF-8",
                Data: "HTML_FORMAT_BODY"
                },
                */
                Text: {
                    Charset: "UTF-8",
                    Data: "we received your Project, your data is under revision" + 
                          "for any updates on your project this token will be " +
                          "available for 24h then your project will finally be submitted" + 
                          "Your Token New : http://reminum.com/submitproject/projectData/" + reset_code
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: emailSubject
            }
        },
        Source: sourceAddress,
        ReplyToAddresses: [replyToAddresses]
    };
    // Create the promise and SES service object
    var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(emailData).promise();
        sendPromise.then(function(data) {
            console.log(data.MessageId);
            return true;
        }).catch(function(err) {
            console.error(err, err.stack);
    });
  }

  verifyCheck(toVerifyAddresses){
    return new Promise((resolve, reject) => {

    var params = {
      Identities: [
        toVerifyAddresses
      ]
    };

    var listIDsPromise = new AWS.SES({apiVersion: '2010-12-01'}).getIdentityVerificationAttributes(params).promise();

    // Handle promise's fulfilled/rejected states
    listIDsPromise.then(
      function(data) {
        var status = data.VerificationAttributes[toVerifyAddresses].VerificationStatus;
        console.log(status);
        resolve(status);
    }).catch(
      function(err) {
        ereject(err.stack);
      });
    });
  } 
}

// /* POST to send the code to reset link */
// router.post('/send-code', async function (req, res, next) {
//     var _reset_code = new ObjectID();

//     var status = await verifyCheck(_toAddresses,res);
//     if(status == "Success"){
//      await sendMail(res, _Ccemail, _replyToAddresses, _toAddresses, _sourceAddress, _textFormatBody, _emailSubject, _reset_code);

//    }else if(status != "Success"){
//       return res.send({res:status});
//    }
// });

// /* Get the token to edit the project data */
// router.get('/projectData/:token', async function (req, res, next) {
//   var project_id = req.params.token;
//   var db = req.db; // Set our internal DB variable
//   var componentscollection = db.get('projects'); // Set our collection
//   var query = {"_id" : ObjectID(project_id) };
//   componentscollection.find(query,function(err, result) {
//     if (err) return res.status(500).json({error: "Internal Server Error"});
//     else{
//       if(result[0])
//       {
//         req.session.projectData = result[0].project_data;
//         req.session.projectData.edit = project_id;
//         req.session.searchData = {
//             searching_string : " ",
//             current_page     : "1"
//         }
//         return res.redirect('http://reminum.com/addproject/finalsubmit');
//       }else{
//         return res.send("Expired token");
//       }
//     }
//   });
// });

module.exports = eMail;