var express = require('express');
var router = express.Router();
var path = require('path');
//var uniqid = require('uniqid');

/* GET home page. */
router.get('/', function(req, res, next) {
  req.session.duser = "logged"
  return res.sendFile(path.join(__dirname + '/../views/login.html'));
});

/* GET home page. */
router.get('/register', function(req, res, next) {
  return res.sendFile(path.join(__dirname + '/../views/register.html'));
});

/* GET home page. */
router.get('/forget-pass', function(req, res, next) {
  return res.sendFile(path.join(__dirname + '/../views/forpass.html'));
});

/* GET logout and bac to login */
router.get('/logout', function(req, res, next) {
  req.session.duser = "";
  return res.sendFile(path.join(__dirname + '/../views/login.html'));
});

/* GET 404 Error */
router.get('/404', function(req, res, next) {
  return res.sendFile(path.join(__dirname + '/../views/404.html'));
});

/* GET 500 Error */
router.get('/500', function(req, res, next) {
  return res.sendFile(path.join(__dirname + '/../views/500.html'));
});

/* GET home page. */
router.get('/charts', function(req, res, next) {
  console.log(req.session);
  if(req.session.duser){
    return res.sendFile(path.join(__dirname + '/../views/charts.html'));
  }else{
    return res.sendFile(path.join(__dirname + '/../views/login.html'));
  }
});

module.exports = router;
