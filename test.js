var path    = require('path');
var fs = require('fs');
var mongo   = require('mongodb');
var config  = require('./config');
ObjectID    = mongo.ObjectID;

var AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: config.AWSconf.accessKeyId,
    secretAccessKey: config.AWSconf.secretAccessKey,
    region: config.AWSconf.region
});


var _Ccemail          = 'ma7moudeyada77@gmail.com';
var _replyToAddresses = 'ma7moudeyada77@gmail.com';
var _toAddresses      = "ma7moudeyada77@yahoo.com";
var _sourceAddress    = 'ma7moudeyada77@gmail.com';
var _textFormatBody   = 'we received your data is under revision';
var _emailSubject     = 'Your Project Verify';

function sendMail(Ccemail, replyToAddresses, toAddresses, sourceAddress, textFormatBody, emailSubject, data_email){
    console.log(Ccemail, replyToAddresses, toAddresses, sourceAddress, textFormatBody, emailSubject, data_email);
    var emailData = {
        Destination: { 
            //CcAddresses: [Ccemail],
            ToAddresses: [toAddresses]
        },
        Message: {
            Body: {
                Html: {
                Charset: "UTF-8",
                Data: data_email
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: emailSubject
            }
        },
        Source: sourceAddress,
        ReplyToAddresses: [replyToAddresses]
    };
    // Create the promise and SES service object
    var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(emailData).promise();
            sendPromise.then(function(data) {
                console.log(data.MessageId);
                return true;
            }).catch(function(err) {
                console.error(err, err.stack);
            });
}

function verifyCheck(toVerifyAddresses){
  return new Promise((resolve, reject) => {

  var params = {
    Identities: [
      toVerifyAddresses
    ]
   };

  var listIDsPromise = new AWS.SES({apiVersion: '2010-12-01'}).getIdentityVerificationAttributes(params).promise();

  // Handle promise's fulfilled/rejected states
  listIDsPromise.then(
    function(data) {
      var status = data.VerificationAttributes[toVerifyAddresses].VerificationStatus;
      console.log(status);
      resolve(status);
   }).catch(
    function(err) {
        reject(err.stack);
    });
  });
}
async function test(){
    var _reset_code = new ObjectID();
    //var status = await verifyCheck(_toAddresses);
    fs.readFile(__dirname + '/emailform.html', 'utf8', async function(err, html){
        var data_email = html;
        await sendMail(_Ccemail, _replyToAddresses, _toAddresses, _sourceAddress, _textFormatBody, _emailSubject, data_email);
    });
}
//test();
verifyCheck("ma7moudeyada77@gmail.com");